
interface IMainUiPresenter {

  void selectTile(String word);
  void speakStatement();
}

interface IMainUiFunctionality {

  void init(IMainUiPresenter presenter);

  void showTiles(List<String> words);

  void clearStack();
  void stackTile(String word);
  void setStatement(String statement);
}
