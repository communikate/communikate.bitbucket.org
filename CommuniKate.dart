#import('dart:html');
#source('Dictionary.dart');
#source('IMainUi.dart');
#source('MainUi.dart');

/*
Released under the AGPL 3.0
http://www.gnu.org/licenses/agpl-3.0.html

Dart gave errors when classes & interfaces were in individual files, so just put
it all in one file and continued.. It'll be sorted out at some point..

Didn't spend much time on the MVP stuff as Dart will provide its own imp with a
later iteration..

Basically, we create the presenter which inits the UI and passes in some dictionary
data. The data is pushed to the UI. User interactions are passed back to the
presenter, handled and the UI updated..

We start by showing popular tiles. As the user selects them, they are racked up
top, and the words are put into a text area. The tiles shown are updated according
to context, and when the user is finished they can click to hear their statment.
*/


void main() {

  MainPresenter mp = new MainPresenter();
}


class MainPresenter implements IMainUiPresenter {

  MainUi ui;
  Dictionary dic;

  String statement;

  MainPresenter() {

    ui = new MainUi();
    dic = new Dictionary();

    statement = "";

    ui.init(this);
    ui.clearStack();
    ui.setStatement('');
    ui.showTiles(dic.getTop(300));
  }


  void selectTile(String word) {

    if(statement == "") {

      statement = word;

    } else {

      statement = "$statement $word";
    }

    ui.setStatement(statement);
    ui.stackTile(word);
  }

  void speakStatement() {
  }
}



class MainUi implements IMainUiFunctionality {

  IMainUiPresenter _presenter;

  UListElement _stackedtiles;
  InputElement _textual;
  InputElement _speak;
  UListElement _tilegrid;

  MainUi() {

    _stackedtiles = query('#stack');
    _textual = query('#textual');
    _speak = query('#speak');
    _tilegrid = query('#tilegrid');
  }

  void init(IMainUiPresenter presenter) {

    _presenter = presenter;

    _speak.on.click.add((Event function) => _presenter.speakStatement());
  }

  void showTiles(List<String> words) {

    _tilegrid.nodes.clear();

    for(String word in words) {

      _tilegrid.nodes.addLast(_generateTile(word));

      SpanElement whitespace = new SpanElement();
      whitespace.text = ' ';
      _tilegrid.nodes.add(whitespace);

    }
  }

  void clearStack() {

    _stackedtiles.nodes.clear();
  }

  void stackTile(String word) {

    _stackedtiles.nodes.addLast(_generateTile(word));
  }

  void setStatement(String statement) {

    _textual.value = statement;
  }

  Element _generateTile(String word) {

    LIElement container = new LIElement();
    container.classes.add('tile');
    container.text = word;

    container.on.click.add((Event function) => _presenter.selectTile(word));

    return container;
  }
}
